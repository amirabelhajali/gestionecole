import { Injectable } from '@angular/core';
import { Candidat } from '../modules/candidat';

@Injectable({
  providedIn: 'root'
})
export class CandidatService {
  candidatList: Array<Candidat> = [];
  constructor() { }

  getAllItems()
  {

    this.candidatList = JSON.parse(localStorage.getItem("users"));
    this.candidatList =  this.candidatList.filter(x => x.role == "candidat");
   // const result = this.candidatList.find(x => x.role == "candidat");
    if (this.candidatList== null) this.candidatList = [];;
     
    return this.candidatList;
  }
  
  getItem(i)
   {
    const candidatList = JSON.parse(localStorage.getItem("users")) || [];

    return candidatList[i];
   }
   getIndexByEmail(item)
   {
    const candidatList = JSON.parse(localStorage.getItem("users")) || [];
    const result = candidatList.findIndex(x => x.email == item);
    console.log('resiltttttt'+result);
    return result;
   }
   
   addItem(item)
   {
     item.fax="null";
     item.role="candidat";
    this.candidatList.push(item);
     localStorage.setItem('users',JSON.stringify(this.candidatList));  }
   
 deleteItem(i) 
 {
    this.candidatList.splice(i,1);
 
     localStorage.setItem('users', JSON.stringify(this.candidatList));
 }
  
  updateItem(i, newItem)
  {
    newItem.fax="null";
    newItem.role="candidat";
    const candidatList = JSON.parse(localStorage.getItem("users")) || [];
    candidatList[i] = newItem;
   localStorage.setItem('users', JSON.stringify(candidatList));

  }
  login(Item)
  {
    const candidatList = JSON.parse(localStorage.getItem("users")) || [];
    const result = candidatList.find(x => x.email == Item.email);
    console.log("result "+candidatList);
    if (result.password == Item.password) {
      return true;
    }
    else {
      return false;
    }
  }
}
