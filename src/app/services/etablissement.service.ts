import { Injectable } from '@angular/core';
import { Etablissement } from '../modules/etablissement';

@Injectable({
  providedIn: 'root'
})
export class EtablissementService {
  etabList: Array<Etablissement> = [];
  resultList: Array<Etablissement> = []
  constructor() { }

  getAllItems()
  {
    this.etabList = JSON.parse(localStorage.getItem("users"));
    this.etabList =  this.etabList.filter(x => x.role == "etablissement");
   // this.resultList = this.etabList.find(x => x.role == "etablissement");
    if (this.etabList == null) this.etabList = [];;

    return this.etabList;
  }
  
  getItem(i)
   {
     
    const etabList = JSON.parse(localStorage.getItem("users")) || [];

    return etabList[i];
   }
   getIndexByEmail(item)
   {
    const etabList = JSON.parse(localStorage.getItem("users")) || [];
    const result = etabList.findIndex(x => x.email == item);
    console.log('resiltttttt'+result);

    return result;
   }
   
   addItem(item)
   {
     item.prenom="null";
    item.role="etablissement";
  this.etabList.push(item);
   localStorage.setItem('users',JSON.stringify(this.etabList));  }
   
 deleteItem(i) 
 {
    this.etabList.splice(i,1);
 
     localStorage.setItem('users', JSON.stringify(this.etabList));
 }
  
  updateItem(i, newItem)
  {
    newItem.prenom="null";
    newItem.role="etablissement";
    const etabList = JSON.parse(localStorage.getItem("users")) || [];
    etabList[i] = newItem;
   localStorage.setItem('users', JSON.stringify(etabList));

  }
  login(Item)
  {
    const etabList = JSON.parse(localStorage.getItem("users")) || [];
    const result = etabList.find(x => x.email == Item.email);
    console.log("result "+etabList);
    if (result.password == Item.password) {
      return true;
    }
    else {
      return false;
    }
  }
}
