import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }
  isLoginSubject = new BehaviorSubject<boolean>(this.hasToken());

  private hasToken() : boolean {
    return !!localStorage.getItem('email');
  }
   getEmail() : String {
    return localStorage.getItem('email');
  }
   getRole() : String {
    return localStorage.getItem('role');
  }
  
  login(email,role) : void {
    localStorage.setItem('token', 'TOKEN');
    localStorage.setItem('email', email);
    localStorage.setItem('role', role);
    this.isLoginSubject.next(true);
  }
    logout() : void {
      localStorage.removeItem('token');
      localStorage.removeItem('email');
      localStorage.removeItem('role');
      this.isLoginSubject.next(false);
    }
    public isAuthenticated(): boolean {  
    const token = localStorage.getItem('token'); 
    if (token) 
    {
      return true;
    }  // Check whether the token is expired and return
    return false;
  }

    isLoggedIn() : Observable<boolean> {
      return this.isLoginSubject.asObservable();
     }

}
