import {Component} from '@angular/core';
import { navItems } from '../../_nav';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { EtablissementService } from '../../services/etablissement.service';
import { CandidatService } from '../../services/candidat.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent {
  public sidebarMinimized = false;
  public navItems = navItems;

  constructor(private _authService:AuthService, private router:Router ){}
  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }
  logout()
  {
    this._authService.logout();
    this.router.navigateByUrl('/login');

  }
  getProfile()
  {
    //const email=this._authService.getEmail();
    const role=this._authService.getRole();
    console.log('roleeee: '+this._authService.getRole());
    if(role=="etablissement")
    {
      this.router.navigateByUrl('/profileEtab');
    }
    else{
    this.router.navigateByUrl('/profileCandidat');
  }
  }
}
