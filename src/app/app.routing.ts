import { NgModule } from '@angular/core';
import { Routes, RouterModule,CanActivate } from '@angular/router';
// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { UpdateEtabComponent } from './views/update-etab/update-etab.component';
import { ProfileEtabComponent } from './views/profile-etab/profile-etab.component';
import { ProfileCandidatComponent } from './views/profile-candidat/profile-candidat.component';
import { UpdateCandidatComponent } from './views/update-candidat/update-candidat.component';
import { AuthGuardService as AuthGuard} from './services/auth-guard.service';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'dashboard',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard]
   
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],

    data: {
      title: 'Home'
    },
    children: [
     {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule),
        canActivate: [AuthGuard]

      },
      {
        path: 'profileCandidat',
        component: ProfileCandidatComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'profile Candidat'
        }
      },
      
    
      {
        path: 'gestionEtab',
        component: UpdateEtabComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'getsion etablissement'
        }
      },
      {
        path: 'gestionCandidat',
        component: UpdateCandidatComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'getsion Candidat'
        }
      },
      {
        path: 'profileEtab',
        component: ProfileEtabComponent,
        canActivate: [AuthGuard],
        data: {
          title: 'profile Etab'
        }
      },
     
      
    ]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
