import { Component, OnInit } from '@angular/core';
import { Candidat } from '../../modules/candidat';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CandidatService } from '../../services/candidat.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-profile-candidat',
  templateUrl: './profile-candidat.component.html',
  styleUrls: ['./profile-candidat.component.css']
})
export class ProfileCandidatComponent implements OnInit {
  candidatForm: FormGroup;
  candidat: Candidat;
  submitted = false;
  index=null;
  constructor(private _candidatService:CandidatService, private _authService:AuthService) { }

  ngOnInit(): void {
    this.candidatForm = new FormGroup({
      nom: new FormControl('', [Validators.required]),
      prenom: new FormControl('', [Validators.required]),
      adresse: new FormControl('', [Validators.required]),
      telephone: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    })
    this.index=this._candidatService.getIndexByEmail(this._authService.getEmail())
    this.candidat=this._candidatService.getItem(this.index);
    console.log("candidat: "+this.candidat);
    this.candidatForm.patchValue(this.candidat);
  }
  get formControls() {
    return this.candidatForm.controls;
  }

  saveUpdate()
  {
   this.submitted = true;
   if(this.candidatForm.invalid) 
   {return;}
   this._candidatService.updateItem(this.index, this.candidatForm.value);
   this.index = null;
   this.submitted = false;
 //  this.candidatForm.patchValue({nom: '', prenom: '', adresse: '', telephone: '', email: '', password: ''});
 //  this._candidatService.getAllItems(); 
  }
  submitImage()
  {
    
  }

}
