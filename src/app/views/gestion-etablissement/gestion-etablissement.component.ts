import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Etablissement } from '../../modules/etablissement';
import { FormGroup } from '@angular/forms';
import { EtablissementService } from '../../services/etablissement.service';

@Component({
  selector: 'app-gestion-etablissement',
  templateUrl: './gestion-etablissement.component.html',
  styleUrls: ['./gestion-etablissement.component.css']
})
export class GestionEtablissementComponent implements OnInit {
  @Output() indexChange = new EventEmitter();
  etabList: Array<Etablissement> = [];
  headElements = ['Nom', 'Adresse', 'Telephone','Fax','email', 'Action'];
  etabForm: FormGroup;
  etab:Etablissement;
  index= null;
  submitted = false;

  constructor(private _etabervice:EtablissementService) { }

  ngOnInit(): void {
    this.etabList = this._etabervice.getAllItems();
  }
  deleteEtab(i)
  {
    
   this._etabervice.deleteItem(i);
   this._etabervice.getAllItems(); 
   }
   updateEtab(i)
   {
    this.indexChange.emit(i);
   }

}
