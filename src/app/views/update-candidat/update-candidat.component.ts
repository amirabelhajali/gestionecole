import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Candidat } from '../../modules/candidat';
import { CandidatService } from '../../services/candidat.service';

@Component({
  selector: 'app-update-candidat',
  templateUrl: './update-candidat.component.html',
  styleUrls: ['./update-candidat.component.css']
})
export class UpdateCandidatComponent implements OnInit {

  candidatForm: FormGroup;
  candidatList: Array<Candidat> = [];
  candidat: Candidat;
  submitted = false;
  index=null;

  constructor(private _candidatService:CandidatService) { }

  ngOnInit(): void {
    this.candidatForm = new FormGroup({
      nom: new FormControl('', [Validators.required]),
      prenom: new FormControl('', [Validators.required]),
      adresse: new FormControl('', [Validators.required]),
      telephone: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    })
  }
  get formControls() {
    return this.candidatForm.controls;
  }
  displayIndex(i)
  {
    this.index=i
    this.candidat=this._candidatService.getItem(this.index);
    this.candidatForm.patchValue(this.candidat);
  //console.log(index);
  }
  onSubmit() {
    this.submitted = true;
    if(this.candidatForm.invalid) 
    {return;}
     
  this._candidatService.addItem(this.candidatForm.value);
  console.log(this.candidatForm.value);
  this.candidatForm.patchValue({nom: '', prenom: '', adresse: '', telephone: '', email: '', password: ''});
  this.submitted = false;
  this.index=null;
}
  saveUpdate()
  {
   this.submitted = true;
   if(this.candidatForm.invalid) 
   {return;}
   this._candidatService.updateItem(this.index, this.candidatForm.value);
   this.index = null;
   this.submitted = false;
   this.candidatForm.patchValue({nom: '', prenom: '', adresse: '', telephone: '', email: '', password: ''});
   this._candidatService.getAllItems(); 
  }
}
