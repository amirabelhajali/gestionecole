import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Etablissement } from '../../modules/etablissement';
import { EtablissementService } from '../../services/etablissement.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-profile-etab',
  templateUrl: './profile-etab.component.html',
  styleUrls: ['./profile-etab.component.css']
})
export class ProfileEtabComponent implements OnInit {
  etabForm: FormGroup;
  etab: Etablissement;
  submitted = false;
  index=null;
  constructor(private _etabService:EtablissementService, private _authService:AuthService) { }

  ngOnInit(): void {
      this.etabForm = new FormGroup({
      nom: new FormControl('', [Validators.required]),
      adresse: new FormControl('', [Validators.required]),
      telephone: new FormControl('', [Validators.required]),
      fax: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    });
    this.index=this._etabService.getIndexByEmail(this._authService.getEmail())
    this.etab=this._etabService.getItem(this.index);
    console.log("etabs: "+this.etab);
    this.etabForm.patchValue(this.etab);
  }
  get formControls() {
    return this.etabForm.controls;
  }

  saveUpdate()
  {
   this.submitted = true;
   if(this.etabForm.invalid) 
   {return;}
   this._etabService.updateItem(this.index, this.etabForm.value);
   this.index = null;
   this.submitted = false;
   this.etabForm.patchValue({nom: '', adresse: '', telephone: '', fax: '', email: '',password:''});
   this._etabService.getAllItems(); 
  }
}
