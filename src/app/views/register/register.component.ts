import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EtablissementService } from '../../services/etablissement.service';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  
  constructor(private _etabService:EtablissementService,private router: Router, private _authService:AuthService) { }

  ngOnInit(): void {
    this.registerForm = new FormGroup({
      nom: new FormControl('', [Validators.required]),
      adresse: new FormControl('', [Validators.required]),
      telephone: new FormControl('', [Validators.required]),
      fax: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    })
  }
  get formControls() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if(this.registerForm.invalid) 
    {return;}
    this._authService.login(this.registerForm.controls ['email'].value,"etablissement");
  this._etabService.addItem(this.registerForm.value);
  this.registerForm.patchValue({nom: '',adresse: '',telephone: '',fax: '',email: '', password: ''});
  this.submitted = false;
  this.router.navigateByUrl('/dashboard');
}
}
