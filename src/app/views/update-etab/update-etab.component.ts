import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Etablissement } from '../../modules/etablissement';
import { EtablissementService } from '../../services/etablissement.service';

@Component({
  selector: 'app-update-etab',
  templateUrl: './update-etab.component.html',
  styleUrls: ['./update-etab.component.css']
})
export class UpdateEtabComponent implements OnInit {

  etabForm: FormGroup;
  etabList: Array<Etablissement> = [];
  etab: Etablissement;
  submitted = false;
  index=null;
  constructor(private _etabService:EtablissementService) { }

  ngOnInit(): void {
    this.etabForm = new FormGroup({
      nom: new FormControl('', [Validators.required]),
      adresse: new FormControl('', [Validators.required]),
      telephone: new FormControl('', [Validators.required]),
      fax: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email])
    })
  }
  get formControls() {
    return this.etabForm.controls;
  }
  displayIndex(i)
  {
    this.index=i
    this.etab=this._etabService.getItem(this.index);
    this.etabForm.patchValue(this.etab);
  //console.log(index);
  }
  saveUpdate()
  {
   this.submitted = true;
   if(this.etabForm.invalid) 
   {return;}
   this._etabService.updateItem(this.index, this.etabForm.value);
   this.index = null;
   this.submitted = false;
   this.etabForm.patchValue({nom: '', adresse: '', telephone: '', fax: '', email: ''});
   this._etabService.getAllItems(); 
  }


}
