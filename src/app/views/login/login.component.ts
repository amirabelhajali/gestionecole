import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EtablissementService } from '../../services/etablissement.service';
import { AuthService } from '../../services/auth.service';
import { Etablissement } from '../../modules/etablissement';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  etab:Etablissement;
   index= null;
  constructor(private router: Router,private _etabervice:EtablissementService, private _authService:AuthService) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    })
  }

  loginUser()
    {
      
     if (this.loginForm.invalid) {
      return;
    }
    if(this._etabervice.login(this.loginForm.value) )
    {
      this.index=this._etabervice.getIndexByEmail(this.loginForm.controls['email'].value);
      this.etab=this._etabervice.getItem(this.index);
      console.log("email: "+this.loginForm.controls['email'].value);
     this._authService.login(this.etab.email,this.etab.role);
     console.log("role: "+this.etab.role);
      this.router.navigateByUrl('/dashboard');
    }
    else
    this.loginForm.patchValue({email: '', password: ''});
  }

 }
