import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Candidat } from '../../modules/candidat';
import { FormGroup } from '@angular/forms';
import { CandidatService } from '../../services/candidat.service';

@Component({
  selector: 'app-gestion-candidat',
  templateUrl: './gestion-candidat.component.html',
  styleUrls: ['./gestion-candidat.component.css']
})
export class GestionCandidatComponent implements OnInit {
  @Output() indexChange = new EventEmitter();
  
  candidatList: Array<Candidat> = [];
  headElements = ['Nom', 'Prenom', 'Adresse','email', 'Action'];
  candidatForm: FormGroup;
  candidat:Candidat;
  index= null;
  submitted = false;

  constructor(private _candidatService:CandidatService) { }

  ngOnInit(): void {
    this.candidatList = this._candidatService.getAllItems();

  }
  deleteCandidat(i)
  {
    
   this._candidatService.deleteItem(i);
   this._candidatService.getAllItems(); 
   }
   updateCandidat(i)
   {
    this.indexChange.emit(i);
   }

}
