import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    title: true,
    name: 'Menu'
  },
  
  
  {
    name: 'Gestion Candidat',
    url: '/gestionCandidat',
    icon: 'icon-star'
  },
  {
    name: 'Etablissement',
    url: '/gestionEtab',
    icon: 'icon-star'
  },

];


