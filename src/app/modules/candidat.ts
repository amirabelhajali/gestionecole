export class Candidat {
    constructor(
        public nom: string,
        public prenom: string,
        public adresse: string,
        public telephone: string,
        public fax: string,
        public email: string,
        public password : string,
        public role : string
    ){}
}
